import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class Parcelle {

    private int id;
    private float surface;
    private Date dateSemis;
    private Date dateRecoltePrevue;
    private EspeceCultivee especeCultivee;
    private ArrayList<TraitementPhytosanitaire> traitements;

    public Parcelle(int id, int surface){
        this.id = id;
        this.surface = surface;
    }

    public Parcelle(int id, float surface, EspeceCultivee especeCultivee, ArrayList<TraitementPhytosanitaire> lesTP) throws ParseException {
        this.id = id;
        this.surface = surface;
        this.especeCultivee = especeCultivee;
        this.traitements = lesTP;
    }

    public Parcelle(int id, float surface, Date dateSemis, Date dateRecoltePrevue, EspeceCultivee especeCultivee, ArrayList<TraitementPhytosanitaire> lesTP) throws ParseException {
        this.id = id;
        this.dateSemis = dateSemis;
        this.dateRecoltePrevue = dateRecoltePrevue;
        this.surface = surface;
        this.especeCultivee = especeCultivee;
        this.traitements = lesTP;
    }

    public float getSurface() {
        return surface;
    }

    public ArrayList<TraitementPhytosanitaire> getLesTraitements(){
        return this.traitements;
    }

    public void ajouteUnTraitement(TraitementPhytosanitaire traitement) {
        traitements.add(traitement);
    }

    public float quantiteTotaleEpandue(){
        float tot = 0;
        for (TraitementPhytosanitaire unTP : traitements) {
            tot += unTP.quantiteAppliquee();
        }
        return tot;
    }

    @Override
    public String toString() {
        return "Parcelle{" +
                "id=" + id +
                ", surface=" + surface + "ha" +
                '}';
    }
}
