import java.util.ArrayList;

public class TraitementEnChamp extends TraitementPhytosanitaire {
    private String id;
    private Parcelle parcelle;
    private Produit produit;
    private ArrayList<Pulverisation> pulverisations;

    public TraitementEnChamp(Parcelle parcelle, Produit produit, ArrayList<Pulverisation> pulverisations){
        super(parcelle, produit);
        this.pulverisations = pulverisations;
    }

    public TraitementEnChamp(String id, Parcelle parcelle, Produit produit, ArrayList<Pulverisation> pulverisations){
        super(id, parcelle, produit);
        this.pulverisations = pulverisations;
    }

    @Override
    public float quantiteAppliquee(){
        float totDosage = 0;
        for (Pulverisation pulverisation : pulverisations){
            totDosage += pulverisation.getDosage();
        }
        return parcelle.getSurface() * totDosage;
    }

    public ArrayList<Pulverisation> getLesPulverisations(){
        return pulverisations;
    }
}
