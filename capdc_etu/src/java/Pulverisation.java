import java.util.Date;

public class Pulverisation {

    private String id;
    private float dosage;
    private Date datePulverisation;

    public Pulverisation(float dosage) {
        this.dosage = dosage;
    }

    public Pulverisation(String unId, Date uneDate, float unDosage) {
        this.id = unId;
        this.datePulverisation = uneDate;
        this.dosage = unDosage;
    }

    public float getDosage() {
        return dosage;
    }
}
