public class TraitementPhytosanitaire {

    private String id;
    private Parcelle parcelle;
    private Produit produit;

    public TraitementPhytosanitaire(Parcelle uneParcelle, Produit unProduit) {
        this.parcelle = uneParcelle;
        this.produit = unProduit;
    }

    public TraitementPhytosanitaire(String unId, Parcelle uneParcelle, Produit unProduit) {
        this.id = unId;
        this.parcelle = uneParcelle;
        this.produit = unProduit;
    }

    public Parcelle getParcelle() {
        return parcelle;
    }

    public Produit getProduit() {
        return produit;
    }

    // TODO : fonction abstraite
    public float quantiteAppliquee() {
        return 0;
    }
}
