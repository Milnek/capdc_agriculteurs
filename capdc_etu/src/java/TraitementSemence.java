public class TraitementSemence extends TraitementPhytosanitaire {
    private String id;
    private Parcelle parcelle;
    private Produit produit;
    private float dosage;

    public TraitementSemence(Parcelle parcelle, Produit produit, float dosage){
        super(parcelle, produit);
        this.dosage = dosage;
    }

    public TraitementSemence(String id, Parcelle parcelle, Produit produit, float dosage){
        super(id, parcelle, produit);
        this.dosage = dosage;
    }

    @Override
    public float quantiteAppliquee(){
        return parcelle.getSurface() * dosage;
    }
}
