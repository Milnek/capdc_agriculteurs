/**
 * Produit phytosanitaire
 */
public class Produit {
    public String id;
    public String libelle;

    public Produit(String libelle) {
        this.libelle = libelle;
    }

    public Produit(String id, String libelle) {
        this.libelle = libelle;
    }

    public String getLibelle() {
        return libelle;
    }

    @Override
    public String toString() {
        return  libelle ;
    }
}
