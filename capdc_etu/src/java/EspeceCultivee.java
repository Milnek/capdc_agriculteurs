public class EspeceCultivee {
    private String id;
    private String libelle;
    private String type;

    public EspeceCultivee(String id, String libelle, String type){
        this.id = id;
        this.libelle = libelle;
        this.type = type;
    }

    public String getType(){return type;}
}
