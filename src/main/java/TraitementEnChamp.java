package main.java;

import java.util.ArrayList;

public class TraitementEnChamp extends TraitementPhytosanitaire {
    private ArrayList<Pulverisation> pulverisations;

    public TraitementEnChamp(Parcelle parcelle, Produit produit, ArrayList<Pulverisation> pulverisations){
        super(parcelle, produit);
        this.pulverisations = pulverisations;
    }

    public float quantiteAppliquee(){
        float totDosage = 0;
        for (Pulverisation pulverisation : pulverisations){
            totDosage += pulverisation.getDosage();
        }
        return getParcelle().getSurface() * totDosage;
    }

    public ArrayList<Pulverisation> getLesPulverisations(){
        return pulverisations;
    }
}
