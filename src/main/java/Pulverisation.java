package main.java;

public class Pulverisation {

    private float dosage;

    public Pulverisation(float dosage) {
        this.dosage = dosage;
    }

    public float getDosage() {
        return dosage;
    }
}
