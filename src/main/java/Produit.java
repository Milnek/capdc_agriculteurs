package main.java;

/**
 * Produit phytosanitaire
 */
public class Produit {
    public String libelle;

    public Produit(String libelle) {
        this.libelle = libelle;
    }

    public String getLibelle() {
        return libelle;
    }

    @Override
    public String toString() {
        return  libelle ;
    }
}
