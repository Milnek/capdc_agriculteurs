package main.java;

public class TraitementSemence extends TraitementPhytosanitaire {
    private float dosage;

    public TraitementSemence(Parcelle parcelle, Produit produit, float dosage){
        super(parcelle, produit);
        this.dosage = dosage;
    }

    public float quantiteAppliquee(){
        return getParcelle().getSurface() * dosage;
    }
}
