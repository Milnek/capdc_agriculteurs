package main.java;

public class TraitementPhytosanitaire {

    private Parcelle parcelle;
    private Produit produit;

    public TraitementPhytosanitaire(Parcelle uneParcelle, Produit unProduit) {
        this.parcelle = uneParcelle;
        this.produit = unProduit;
    }

    public Parcelle getParcelle() {
        return parcelle;
    }

    public Produit getProduit() {
        return produit;
    }
}
