package main.java;

public class Parcelle {

    private int id;
    private float surface;

    public Parcelle(int id, float surface) {
        this.id = id;
        this.surface = surface;
    }

    public float getSurface() {
        return surface;
    }

    @Override
    public String toString() {
        return "Parcelle{" +
                "id=" + id +
                ", surface=" + surface + "ha" +
                '}';
    }
}
